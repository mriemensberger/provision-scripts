#!/bin/sh

log=/var/log/engine-install-main.log
docker_version=20.10

rm -f "$log"
mkdir -p `dirname "$log"`
exec 1>>"$log" 2>>"$log"

set -ex

echo "## Start engine-install"

export DEBIAN_FRONTEND=noninteractive
apt-get update -y
apt-get upgrade -y 
apt-get install -y --no-install-recommends qemu-user-static binfmt-support

echo "## Start real docker engine install"

curl -L "https://releases.rancher.com/install-docker/$docker_version.sh" | sh -
systemctl restart docker.service

echo "## Register docker restart hook"

nohup /bin/bash -c \
    'until find /etc/systemd/system/docker.service.d/10-machine.conf &>/dev/null; do sleep 1; done; sleep 2; echo "## Hook: Restarting docker"; systemctl reload docker; systemctl restart docker' \
    </dev/null 1>>"$log" 2>>"$log" &

echo "## Completed engine-install"

